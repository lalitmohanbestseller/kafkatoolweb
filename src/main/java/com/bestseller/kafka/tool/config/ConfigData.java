package com.bestseller.kafka.tool.config;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class ConfigData {

    private String brokerUrls;
}
