package com.bestseller.kafka.tool.models.response;

import com.bestseller.kafka.tool.models.enums.ConnectionStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ConnectionCheckResponse {
    private ConnectionStatus connectionStatus;
    private List<String> topics;
}
