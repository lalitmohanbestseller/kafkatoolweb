package com.bestseller.kafka.tool.models.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class MessageQueryRequest {
    private String parameter;
    private String value;
    private String topic;
}
