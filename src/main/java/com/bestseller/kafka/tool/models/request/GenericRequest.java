package com.bestseller.kafka.tool.models.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class GenericRequest {
    private ConnectionCheckRequest connectionCheckRequest;
    private MessageQueryRequest messageQueryRequest;
    private MessagePostRequest messagePostRequest;
}
