package com.bestseller.kafka.tool.exceptions;

public class AthenaQueryException extends Exception {

    public AthenaQueryException(String message) {
        super(message);
    }

}
