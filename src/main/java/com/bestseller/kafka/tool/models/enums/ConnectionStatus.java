package com.bestseller.kafka.tool.models.enums;

public enum ConnectionStatus {
    CONNECTION_OK,
    CONNECTION_FAILED
}
