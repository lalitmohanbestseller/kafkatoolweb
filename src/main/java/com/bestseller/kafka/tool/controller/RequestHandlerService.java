package com.bestseller.kafka.tool.controller;

import com.bestseller.kafka.tool.models.request.GenericRequest;
import com.bestseller.kafka.tool.models.response.MessageQueryResponse;
import com.bestseller.kafka.tool.services.athena.AthenaQueryService;
import com.bestseller.kafka.tool.services.kafka.KafkaService;
import com.bestseller.kafka.tool.models.response.GenericResponse;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class RequestHandlerService {

    @Autowired
    private AthenaQueryService athenaQueryService;

    @Autowired
    KafkaService kafkaService;

    public GenericResponse handleRequest(GenericRequest genericRequest) {
        GenericResponse response = new GenericResponse();
        if (genericRequest.getMessageQueryRequest() != null) {
            MessageQueryResponse messageQueryResponse = athenaQueryService.executeQuery(genericRequest.getMessageQueryRequest());
            response.setMessageQueryResponse(messageQueryResponse);
        }
        if (genericRequest.getConnectionCheckRequest() != null) {
            response.setConnectionCheckResponse(kafkaService.checkConnection());
        }
        if (genericRequest.getMessagePostRequest() != null) {
            response.setMessagePostResponse(kafkaService.postMessage(genericRequest.getMessagePostRequest()));
        }
        log.info("Sending Response {}", response.toString());
        return response;
    }

}
