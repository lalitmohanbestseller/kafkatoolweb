package com.bestseller.kafka.tool.models.request;

import com.bestseller.kafka.tool.models.common.RowData;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class MessagePostRequest {
    private List<RowData> rowDataList = new ArrayList<>();
}
