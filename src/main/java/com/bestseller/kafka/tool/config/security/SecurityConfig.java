package com.bestseller.kafka.tool.config.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.ldap.core.support.BaseLdapPathContextSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.ldap.DefaultSpringSecurityContextSource;
import org.springframework.security.ldap.authentication.BindAuthenticator;
import org.springframework.security.ldap.authentication.LdapAuthenticationProvider;
import org.springframework.security.ldap.search.FilterBasedLdapUserSearch;
import org.springframework.security.ldap.userdetails.DefaultLdapAuthoritiesPopulator;
import org.springframework.security.ldap.userdetails.LdapAuthoritiesPopulator;

import java.util.Collections;


/**
 * Configuration for Spring Security. Secures the static and api endpoints. Credentials for the USER role are
 * injected through the system properties: user.email & user.password.
 */
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Value("${ldap.host}")
    private String ldapHost;
    @Value("${ldap.user.searchbase}")
    private String ldapUserSearchBase;
    @Value("${ldap.user.searchfilter}")
    private String ldapUserSearchFilter;
    @Value("${ldap.managerdn}")
    private String ldapManagerDN;
    @Value("${ldap.manager.password}")
    private String ldapManagerPassword;

    /**
     * Configures the http security.
     *
     * @param security Security Object
     *
     * @throws Exception Exception if something goes wrong
     */
    @Override
    protected void configure(HttpSecurity security) throws Exception {
        security.authorizeRequests()
                .antMatchers("/login.html" , "/loginUser" , "/assets/**").permitAll()
                .anyRequest().authenticated()
                .and().formLogin()
                .loginPage("/login.html").permitAll()
                .defaultSuccessUrl("/index.html", true)
                .and().csrf().disable();
    }

    /**
     * LDAP identification.
     *
     * @param auth
     * @throws Exception
     */

    @Override
    public void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(authenticationProvider());
    }

    @Bean
    public BindAuthenticator bindAuthenticator() {
        final BindAuthenticator bindAuthenticator = new BindAuthenticator(contextSource());
        bindAuthenticator.setUserSearch(userSearch());
        return bindAuthenticator;
    }

    @Bean
    public BaseLdapPathContextSource contextSource() {
        DefaultSpringSecurityContextSource contextSource = new DefaultSpringSecurityContextSource(ldapHost);
        contextSource.setUserDn(ldapManagerDN);
        contextSource.setPassword(ldapManagerPassword);
        return contextSource;
    }

    @Bean
    public FilterBasedLdapUserSearch userSearch() {
        return new FilterBasedLdapUserSearch(ldapUserSearchBase, ldapUserSearchFilter, contextSource());
    }

    @Bean
    public AuthenticationManager authenticationManager() {
        ProviderManager providerManager = new ProviderManager(Collections.singletonList(authenticationProvider()));
        providerManager.isEraseCredentialsAfterAuthentication();
        return providerManager;
    }

    @Bean
    public AuthenticationProvider authenticationProvider() {
        return new LdapAuthenticationProvider(bindAuthenticator(), populator());
    }

    @Bean
    public LdapAuthoritiesPopulator populator() {
        DefaultLdapAuthoritiesPopulator populator =
                new DefaultLdapAuthoritiesPopulator(contextSource(), ldapUserSearchBase); //this should be group search base.
        populator.setSearchSubtree(true);
        populator.setIgnorePartialResultException(true);
        populator.setGroupSearchFilter(ldapUserSearchFilter);
        return populator;
    }

}
