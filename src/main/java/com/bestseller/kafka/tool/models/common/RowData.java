package com.bestseller.kafka.tool.models.common;

import com.bestseller.kafka.tool.models.enums.MessagePostStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RowData {

    private String time;
    private String topic;
    private String orderId;
    private String payload;
    private MessagePostStatus messagePostStatus;


}
