var current_value = '9110460917,9210084816,9410084734';

module.controller('baseController', function($scope, $window, $http) {
	$scope.select = function(item, model) {
		$scope[model] = item;
	};

	$scope.logOut = function(){
		swal({
				title: "Are you sure?",
				text: "You will be redirected to the login page",
				type: "warning",
				showCancelButton: true,
				confirmButtonClass: "btn-danger",
				confirmButtonText: "Yes, Log Out!",
				closeOnConfirm: false
			},
			function(){
			$http.get("/logoutUser")
				.then(function successCallback(){
					swal({
						title: "Redirecting!",
						text: "You will be logged out soon.",
						type: "success",
						showCancelButton: false,
						showConfirmButton: false,
						closeOnConfirm: false
					});
					setTimeout(function(){
						$window.location.href = "/login.html"
					},1500);
				});

			});
	}

});

// Configure.
module.controller('configController', function($scope, $rootScope, $http) {

	$scope.isShow = false;
	$scope.msg = '';
	$scope.alertclass = '';

	$scope.imageicon = 'assets/images/1.png';
	$scope.pingmessage = null;
	$scope.brokerResponse = null;

	$rootScope.topicList = null;


	$scope.post = function() {
		$scope.msg = 'Configuration applied!';
		$scope.alertclass = 'alert-success';
		$scope.isShow = true;
	};
	$scope.hide = function() {
		$scope.isShow = false;
	};

	$scope.ping = function() {
		var dataObj = {
			connectionCheckRequest : {
				check : 'checkConnection'
			}
		}
		var endpoint = window.location.origin + "/invoke";
		var res = $http.post(endpoint, dataObj);
		res.success(function(data) {
			$scope.brokerResponse = data;
			if ($scope.brokerResponse.connectionCheckResponse.connectionStatus==='CONNECTION_OK') {
				$rootScope.topicList = $scope.brokerResponse.connectionCheckResponse.topics;
				$scope.imageicon = 'assets/images/2.png';
				$scope.pingmessage = 'Connection OK & topics are retrieved';
			} else {
				$rootScope.topicList = '';
				$scope.imageicon = "assets/images/1.png";
				$scope.pingmessage = 'Connection failed';
			}

		}).error(function() {
			$rootScope.topicList = '';
			$scope.imageicon = 'assets/images/1.png';
			$scope.pingmessage = 'Connection failed';
		});
	};

});

// Query Athena for message payload.
module.controller('messageQueryController', function($scope, $rootScope, $http) {

	$scope.isShow = false;
	$scope.msg = '';
	$scope.alertclass = '';

	$scope.imageicon = 'assets/images/1.png';
	$scope.pingmessage = null;
	$scope.brokerResponse = null;

	$scope.parameter;
	$scope.paramvalue = current_value;
	$scope.topic;

	$rootScope.querydatalist;

	$scope.ping = function() {
		var dataObj = {
			connectionCheckRequest : {
				check : 'checkConnection'
			}
		}
		var endpoint = window.location.origin + "/invoke";
		var res = $http.post(endpoint, dataObj);
		res.success(function(data) {
			$scope.brokerResponse = data;
			if ($scope.brokerResponse.connectionCheckResponse.connectionStatus==='CONNECTION_OK') {
				$rootScope.topicList = $scope.brokerResponse.connectionCheckResponse.topics;
				$scope.imageicon = 'assets/images/2.png';
				$scope.pingmessage = 'Connection OK';
			} else {
				$rootScope.topicList = '';
				$scope.imageicon = "assets/images/1.png";
				$scope.pingmessage = 'Connection failed';
			}

		}).error(function() {
			$rootScope.topicList = '';
			$scope.imageicon = 'assets/images/1.png';
			$scope.pingmessage = 'Connection failed';
		});
	};

	$scope.post = function() {
		if ($scope.parameter && $scope.topic) {

			var dataObj = {
				messageQueryRequest : {
					parameter : $scope.parameter,
					value : $scope.paramvalue,
					topic : $scope.topic
				}
			};
			getData(dataObj);
		} else if (!$scope.parameter) {
			$scope.msg = 'Please select parameter.';
			$scope.alertclass = 'alert-danger';
			$scope.isShow = true;
		} else if (!$scope.topic){
			$scope.msg = 'Please select topic.';
			$scope.alertclass = 'alert-danger';
			$scope.isShow = true;
		}

	};
	function getData(dataObj) {
		var endpoint = window.location.origin + "/invoke"; // gets hostname and port of service that delivered file
		var res = $http.post(endpoint, dataObj);
		res.success(function(data) {
			if(data.messageQueryResponse.rowDataList.length>0){
				$scope.msg = 'Message retrieved';
				$scope.alertclass = 'alert-success';
				$scope.isShow = true;
				$rootScope.querydatalist = data.messageQueryResponse.rowDataList;
			}else{
				$rootScope.querydatalist='';
				$scope.msg = 'Message not found';
				$scope.alertclass = 'alert-danger';
				$scope.isShow = true;
			}

		}).error(function() {
			$rootScope.querydatalist='';
			$scope.msg = 'Error occured/connection refused.';
			$scope.alertclass = 'alert-danger';
			$scope.isShow = true;
		});
	}
	$scope.hide = function() {
		$scope.isShow = false;
	}
});

// Post message to selected topic.
module.directive('fileModel', ['$parse', function ($parse) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			var model = $parse(attrs.fileModel);
			var modelSetter = model.assign;
			element.bind('change', function(){
				scope.$apply(function(){
					modelSetter(scope, element[0].files[0]);
				});
			});
		}
	};
}]);

module.controller('messageSenderController', function($scope, $rootScope, $http, $filter) {

	$scope.isShow = true;
	$scope.msg = '';
	$scope.alertclass = '';

	$scope.selectedRows = [];
    $scope.updatedPayload = [];

	$scope.select = function(item) {
		item.selected ? item.selected = false : item.selected = true;
	};

    $scope.update = function (index) {
        $rootScope.querydatalist[index].payload = $scope.updatedPayload[index];
		$scope.updatedPayload[index] = row
    };

    $scope.copyRow = function (index,payload) {
		$scope.updatedPayload[index] = payload;
	};

	$scope.postSelectedRows = function() {
		var selectedRows = $filter("filter")($rootScope.querydatalist, {
			selected: true
		}, true);

		$scope.selectedRows = selectedRows;

		if ($scope.selectedRows.length > 0) {
			var dataObj = {
				messagePostRequest : {
					rowDataList : $scope.selectedRows
				}
			};
			getPostMessageData(dataObj);
		} else {
			$scope.msg = 'Select at least one row';
			$scope.alertclass = 'alert-danger';
			$scope.isShow = true;
		}
	};


	function getPostMessageData(dataObj) {
		var endpoint = window.location.origin + "/invoke"; // gets hostname and port of service that delivered file
		var res = $http.post(endpoint, dataObj);
		res.success(function(data) {
			var full_size = $rootScope.querydatalist.length;
			var sel_size = data.messagePostResponse.rowDataList.length;
			//For updating status
			for(var i =0; i<sel_size; i++){
				for(var j =0; j<full_size; j++){
					var expectedId = $rootScope.querydatalist[j].topic + $rootScope.querydatalist[j].orderId
					var returnedId = data.messagePostResponse.rowDataList[i].topic + data.messagePostResponse.rowDataList[i].orderId
					if(expectedId===returnedId){
						$rootScope.querydatalist[j].messagePostStatus =
							data.messagePostResponse.rowDataList[i].messagePostStatus;
					}
				}
			}
		}).error(function() {
			$scope.msg = 'Error in posting the messages';
			$scope.alertclass = 'alert-danger';
			$scope.isShow = true;
		});
	}
	$scope.hide = function() {
		$scope.isShow = false;
	}

	$scope.postBulkData = function() {
		var endpoint = window.location.origin + "/bulkpost"; // gets hostname and port of service that delivered file
		var file = $scope.myFile;
		var fd = new FormData();
		fd.append('file', file);

		$http.post(endpoint, fd, {
			transformRequest: angular.identity,
			headers: {
				'Content-Type': undefined
			}
		}).success(function(data) {
				$rootScope.querydatalist= data.messagePostResponse.rowDataList;
				$scope.msg = "Upload Successful!";
				$scope.alertclass = 'alert-success';
				$scope.isShow = true;
		}).error(function(){
				$scope.msg = 'Error in bulk posting the messages! Check the CSV format';
				$scope.alertclass = 'alert-danger';
				$scope.isShow = true;
		});
	}
});


// Clear the log panel.
module.controller('logsController', function($scope, $rootScope){

	$scope.clearlogs = function() {
		$rootScope.poststatus = '';
		$rootScope.message = '';
	}

});

// Retrieve last message posted - To be implemented correctly.
module.controller('lastMessageController', function($scope, $rootScope, $http){

	$scope.isShow = false;
	$scope.msg = '';
	$scope.alertclass = '';
	$scope.message;
	$scope.resp;
	$scope.lastmessage=false

	$scope.showLastMessage = function() {
		var endpoint = window.location.origin + "/getLastMessage"; // gets hostname and port of service that delivered file
		if ($scope.lastmessage) {
			var res = $http.post(endpoint, $scope.topic);
			res.success(function(data, status, headers, config) {
				$scope.resp = data;
				$scope.message = $scope.resp;
			}).error(function(data, status, headers, config) {
				$scope.msg = 'Error occured/connection refused.';
				$scope.alertclass = 'alert-danger';
				$scope.isShow = true;
			});
		} else {
			$scope.message = '';
		}
	}
	$scope.hide = function() {
		$scope.isShow = false;
	}

});

