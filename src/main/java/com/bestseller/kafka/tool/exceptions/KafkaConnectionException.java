package com.bestseller.kafka.tool.exceptions;

public class KafkaConnectionException  extends Exception {

    public KafkaConnectionException(String message) {
        super(message);
    }
}
