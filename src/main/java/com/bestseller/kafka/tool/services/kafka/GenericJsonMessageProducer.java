package com.bestseller.kafka.tool.services.kafka;

import com.bestseller.kafka.tool.config.ConfigData;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.LongSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;

import static org.apache.kafka.clients.producer.ProducerConfig.BOOTSTRAP_SERVERS_CONFIG;
import static org.apache.kafka.clients.producer.ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG;
import static org.apache.kafka.clients.producer.ProducerConfig.RETRIES_CONFIG;
import static org.apache.kafka.clients.producer.ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG;

/**
 * A generic producer that would post strings to a topic with a given name.
 */
@Component
@AllArgsConstructor
public class GenericJsonMessageProducer {

    @Autowired
    private ConfigData configData;

    /**
     * Posts a text message to the topic.
     * @param topic
     * @param message
     * @throws Exception
     */
    public <T> void postMessage(String topic, String message) throws Exception {
        Future<RecordMetadata> recordMetadataFuture = kafkaJsonProducer().send(new ProducerRecord(topic, message));
        kafkaJsonProducer().flush();
        recordMetadataFuture.get();
    }


    public Map<String, Object> producerConfigs() {
        Map<String, Object> props = new HashMap<>();
        props.put(BOOTSTRAP_SERVERS_CONFIG, configData.getBrokerUrls());
        props.put(RETRIES_CONFIG, 10);
        props.put(KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class.getName());
        props.put(VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        return props;
    }


    public KafkaProducer kafkaJsonProducer() {
        return new KafkaProducer<Long, String>(producerConfigs());
    }

    public ProducerFactory<Long, Object> kafkaJsonProducerFactory() {
        JsonSerializer<Object> valueSerializer = new JsonSerializer<>(objectMapper());
        valueSerializer.setAddTypeInfo(false);
        return new DefaultKafkaProducerFactory<>(producerConfigs(), new LongSerializer(), valueSerializer);
    }

    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(MapperFeature.DEFAULT_VIEW_INCLUSION, false);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.findAndRegisterModules();
        return objectMapper;
    }

}
