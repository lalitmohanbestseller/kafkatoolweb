package com.bestseller.kafka.tool.models.response;


import com.bestseller.kafka.tool.models.common.RowData;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class MessageQueryResponse {
    private List<RowData> rowDataList;
}
