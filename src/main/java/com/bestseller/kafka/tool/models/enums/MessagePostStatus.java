package com.bestseller.kafka.tool.models.enums;

public enum  MessagePostStatus {
    READY,
    POSTED,
    POST_FAILED
}

