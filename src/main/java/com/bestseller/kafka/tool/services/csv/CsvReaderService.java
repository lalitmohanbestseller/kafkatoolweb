package com.bestseller.kafka.tool.services.csv;

import com.bestseller.kafka.tool.models.common.RowData;
import com.bestseller.kafka.tool.models.request.MessagePostRequest;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

@Component
@AllArgsConstructor
@Slf4j
public class CsvReaderService {
    public MessagePostRequest loadData(MultipartFile multipartFile) {
        String line = "";
        boolean headerSkipped = false;
        MessagePostRequest messagePostRequest = new MessagePostRequest();

        try (BufferedReader br = new BufferedReader(new InputStreamReader(multipartFile.getInputStream()))) {

            while ((line = br.readLine()) != null) {

                //Skipping the headerSkipped
                if(!headerSkipped) {
                    headerSkipped = true;
                    continue;
                }
                // use comma as separator
                String[] row = line.split(";");
                if(row == null || row.length==0){
                    log.warn("Empty row found and skipping the line");
                    continue;
                }
                RowData rowData = new RowData();
                rowData.setTime(row[0]);
                rowData.setTopic(row[1]);
                rowData.setOrderId(row[2]);
                rowData.setPayload(row[3]);
                messagePostRequest.getRowDataList().add(rowData);
            }
        } catch (FileNotFoundException e) {
            log.error("File not found", e);
        } catch (IOException e) {
            log.error("Couldn't open file", e);
        }
        return messagePostRequest;
    }
}
