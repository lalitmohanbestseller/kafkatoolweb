package com.bestseller.kafka.tool.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import software.amazon.awssdk.auth.credentials.AwsCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.athena.AthenaClient;
import software.amazon.awssdk.services.athena.AthenaClientBuilder;

@Configuration
public class AthenaConfig {

    @Value("${aws_access_key_id}")
    private String accessKeyId;

    @Value("${aws_secret_access_key}")
    private String secretAccessKey;

    @Bean
    public AthenaClientBuilder athenaClientBuilder(){

        return AthenaClient.builder().region(Region.EU_WEST_1)
                .credentialsProvider(awsCredentialsProvider());
    }

    @Bean
    public AthenaClient athenaClient() {
        return athenaClientBuilder().build();
    }

    @Bean
    public AwsCredentialsProvider awsCredentialsProvider(){
        AwsCredentialsProvider awsCredentialsProvider = new AwsCredentialsProvider() {
            @Override
            public AwsCredentials resolveCredentials() {
                return new AwsCredentials() {
                    @Override
                    public String accessKeyId() {
                        return accessKeyId;
                    }

                    @Override
                    public String secretAccessKey() {
                        return secretAccessKey;
                    }
                };
            }
        };

        return awsCredentialsProvider;
    }
}
