var module = angular.module('module', [ 'angular-loading-bar' ])
		.config([ 'cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
			cfpLoadingBarProvider.includeBar = false;
		} ])
