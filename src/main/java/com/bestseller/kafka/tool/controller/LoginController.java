package com.bestseller.kafka.tool.controller;

import com.bestseller.kafka.tool.exceptions.ForbiddenException;
import com.bestseller.kafka.tool.models.request.LoginRequest;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@RestController
@Slf4j
@AllArgsConstructor
public class LoginController {

    private AuthenticationManager authManager;

    @RequestMapping(value="/loginUser", method = RequestMethod.POST)
    public Authentication loginUser(@RequestBody LoginRequest loginRequest, final HttpServletRequest request) throws ForbiddenException {

        UsernamePasswordAuthenticationToken authReq =
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(),loginRequest.getPassword());

        try {

            Authentication auth = authManager.authenticate(authReq);
            SecurityContext securityContext = SecurityContextHolder.getContext();
            securityContext.setAuthentication(auth);

            HttpSession session = request.getSession(true);
            session.setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, securityContext);

            return auth;

        } catch (Exception e) {
            throw new ForbiddenException("Authentication Failed.");
        }
    }

    @RequestMapping(value="/logoutUser", method = RequestMethod.GET)
    public void logoutUser(HttpServletRequest request, HttpServletResponse response) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication != null){
            new SecurityContextLogoutHandler().logout(request, response, authentication);
        }

        response.setHeader("Location", "/login.html");
        response.setStatus(HttpStatus.TEMPORARY_REDIRECT.value());

    }

}
