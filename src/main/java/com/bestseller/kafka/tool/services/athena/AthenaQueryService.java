package com.bestseller.kafka.tool.services.athena;

import com.bestseller.kafka.tool.exceptions.AthenaQueryException;
import com.bestseller.kafka.tool.models.common.RowData;
import com.bestseller.kafka.tool.models.enums.MessagePostStatus;
import com.bestseller.kafka.tool.models.request.MessageQueryRequest;
import com.bestseller.kafka.tool.models.response.MessageQueryResponse;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.text.StringSubstitutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.services.athena.AthenaClient;
import software.amazon.awssdk.services.athena.model.GetQueryExecutionRequest;
import software.amazon.awssdk.services.athena.model.GetQueryExecutionResponse;
import software.amazon.awssdk.services.athena.model.GetQueryResultsRequest;
import software.amazon.awssdk.services.athena.model.GetQueryResultsResponse;
import software.amazon.awssdk.services.athena.model.QueryExecutionContext;
import software.amazon.awssdk.services.athena.model.QueryExecutionState;
import software.amazon.awssdk.services.athena.model.ResultConfiguration;
import software.amazon.awssdk.services.athena.model.Row;
import software.amazon.awssdk.services.athena.model.StartQueryExecutionRequest;
import software.amazon.awssdk.services.athena.model.StartQueryExecutionResponse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class AthenaQueryService {

    @Value("${athena.database}")
    private String ATHENA_DATABASE;

    @Value("${athena.output.bucketName}")
    private String ATHENA_OUTPUT_S3_FOLDER_PATH;

    @Value("${athena.sleep_amount_ms}")
    private long SLEEP_AMOUNT_IN_MS;

    @Value("${select.query.any.topic}")
    private String athenaQueryTemplate;

    @Value("${select.query.history}")
    private String historyQueryTemplate;


    @Autowired
    private AthenaClient athenaClient;

    public MessageQueryResponse executeQuery(MessageQueryRequest messageQueryRequest) {

        String formattedQuery;
        try {
            Map<String, String> values = new HashMap<>();

            String orderIds[] = messageQueryRequest.getValue().split(",");
            String value = "";
            int index = 0;
            for (String orderId : orderIds) {
                if (index == orderIds.length - 1) {
                    value = value.concat("'").concat(orderId).concat("'");
                } else {
                    value = value.concat("'").concat(orderId).concat("',");
                }
                index++;

            }
            values.put("value", value);
            values.put("topic", messageQueryRequest.getTopic());
            values.put("parameter", messageQueryRequest.getParameter());

            if (messageQueryRequest.getTopic().equals("History")) {
                formattedQuery = StringSubstitutor.replace(historyQueryTemplate, values, "{", "}");
            } else {
                formattedQuery = StringSubstitutor.replace(athenaQueryTemplate, values, "{", "}");
            }

            log.info("Athena Query : {}", formattedQuery);

            String queryExecutionId = submitAthenaQuery(formattedQuery);

            log.debug("Query submitted : {}", System.currentTimeMillis());

            waitForQueryToComplete(queryExecutionId);

            log.debug("Query finished : {}", System.currentTimeMillis());

            List<RowData> rowDataList = processResultRows(queryExecutionId);

            log.debug("Message from Athena : {}", rowDataList);

            return new MessageQueryResponse(rowDataList);
        } catch (AthenaQueryException | InterruptedException e) {
            log.error("Error" + e.getMessage());
            return new MessageQueryResponse(Collections.singletonList(new RowData("No Data", "No Data", "No Data", e.getMessage(), null)));
        }
    }

    private String submitAthenaQuery(String query) {

        QueryExecutionContext queryExecutionContext = QueryExecutionContext.builder()
                .database(ATHENA_DATABASE).build();

        ResultConfiguration resultConfiguration = ResultConfiguration.builder()
                .outputLocation(ATHENA_OUTPUT_S3_FOLDER_PATH).build();

        StartQueryExecutionRequest startQueryExecutionRequest = StartQueryExecutionRequest.builder()
                .queryString(query)
                .queryExecutionContext(queryExecutionContext)
                .resultConfiguration(resultConfiguration).build();

        StartQueryExecutionResponse startQueryExecutionResponse = athenaClient.startQueryExecution(startQueryExecutionRequest);

        return startQueryExecutionResponse.queryExecutionId();
    }

    private void waitForQueryToComplete(String queryExecutionId) throws AthenaQueryException, InterruptedException {

        GetQueryExecutionRequest getQueryExecutionRequest = GetQueryExecutionRequest.builder()
                .queryExecutionId(queryExecutionId).build();

        GetQueryExecutionResponse getQueryExecutionResponse;

        boolean isQueryStillRunning = true;

        while (isQueryStillRunning) {
            getQueryExecutionResponse = athenaClient.getQueryExecution(getQueryExecutionRequest);
            String queryState = getQueryExecutionResponse.queryExecution().status().state().toString();

            if (queryState.equals(QueryExecutionState.FAILED.toString())) {
                throw new AthenaQueryException("Query Failed to run with Error Message: " + getQueryExecutionResponse
                        .queryExecution().status().stateChangeReason());
            } else if (queryState.equals(QueryExecutionState.CANCELLED.toString())) {
                throw new AthenaQueryException("Query was cancelled.");
            } else if (queryState.equals(QueryExecutionState.SUCCEEDED.toString())) {
                isQueryStillRunning = false;
            } else {
                Thread.sleep(SLEEP_AMOUNT_IN_MS);
            }

            log.debug("Current Status is {} ", queryState);
        }
    }

    private List<RowData> processResultRows(String queryExecutionId) throws AthenaQueryException {

        GetQueryResultsRequest getQueryResultsRequest = GetQueryResultsRequest.builder()
                .queryExecutionId(queryExecutionId).build();
        List<RowData> rowDataList = new ArrayList<>();
        GetQueryResultsResponse getQueryResultsResponse = athenaClient.getQueryResults(getQueryResultsRequest);
        List<Row> rows = getQueryResultsResponse.resultSet().rows();
        if (!rows.isEmpty()) {
            rows = rows.subList(1, rows.size());
            rows.stream().forEach(row -> {
                RowData q = new RowData();
                q.setTime(row.data().get(0).varCharValue());
                q.setTopic(row.data().get(1).varCharValue());
                q.setOrderId(row.data().get(2).varCharValue());
                q.setPayload(row.data().get(3).varCharValue());
                q.setMessagePostStatus(MessagePostStatus.READY);
                rowDataList.add(q);
            });
            return rowDataList;
        }
        throw new AthenaQueryException("No Record found for given criteria");
    }
}
