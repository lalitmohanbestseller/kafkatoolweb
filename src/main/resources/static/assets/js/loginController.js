var app = angular.module('login_app', []);

app.controller('main_controller', function($scope, $location, $window, $http){
    /**
     * Define Globals
     */
    $scope.endpoint = window.location.origin + "/loginUser";
    $scope.username = null;
    $scope.password = null;


    $scope.send_credentials = function() {
        if($scope.username && $scope.password){

            var request = {
                method: 'POST',
                url:  $scope.endpoint,
                data : {
                    'username': $scope.username,
                    'password' : $scope.password
                }
            };

            $http(request)
                .then(
                    function successCallback() {
                        $window.location.href = window.location.origin + '/index.html';
                    },
                    function failureCallback(){
                        swal("Login Error!", "Authentication Failed! Check your username and password.", "error");
                    });
        }
        else {
            swal("Can't Log In!", "Please enter a username and password", "warning")
        }
    }
});
