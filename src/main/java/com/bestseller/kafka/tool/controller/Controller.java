package com.bestseller.kafka.tool.controller;

import com.bestseller.kafka.tool.models.request.GenericRequest;
import com.bestseller.kafka.tool.models.response.GenericResponse;
import com.bestseller.kafka.tool.services.csv.CsvReaderService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@RestController
@Slf4j
@AllArgsConstructor
public class Controller {

    private RequestHandlerService requestHandlerService;
    private CsvReaderService  csvReaderService;

    @RequestMapping("/invoke")
    public @ResponseBody
    GenericResponse invoke(
            @RequestBody GenericRequest genericRequest, HttpServletRequest request) {
        log.info("User {} issued request {}", getLoggedInUser(request), genericRequest.toString());
        return requestHandlerService.handleRequest(genericRequest);
    }

    @PostMapping("/bulkpost")
    public @ResponseBody
    GenericResponse bulkPost(@RequestParam("file") MultipartFile multipartFile, HttpServletRequest request) {
        log.info("User {} issued bulk post request with file {}",getLoggedInUser(request), multipartFile.getOriginalFilename());

        //Convert multipart file to generic request
        GenericRequest genericRequest = new GenericRequest();

        genericRequest.setMessagePostRequest(csvReaderService.loadData(multipartFile));

        return requestHandlerService.handleRequest(genericRequest);

    }

    private String getLoggedInUser(HttpServletRequest request) {
        Principal principal = request.getUserPrincipal();
        return request.getUserPrincipal().getName();
    }
}
