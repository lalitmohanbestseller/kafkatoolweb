# Kafka Tool

Kafka producer for sending messages to selected topic.

## Prerequisites
- Apache Kafka installed (0.10.1.0)

## Local environment configuration

 - Go to Run -> Edit Configurations
 - Create a new Spring Boot configuration (recommended)
    - Spring Boot:
        - Settings:
            - Main class: com.bestseller.kafka.tool.Application
            - VM Options:
                - -Dspring.profiles.active=dev
    - Do Maven clean, install
 - You can run applications now



## Developing and testing using Docker
Instead of installing Apache Kafka on your local machine you can run them inside Docker containers.

#### Local development
In this mode you run the infrastructure components inside Docker containers and the Kafka Tool inside
an IDE of your choice where you can debug, test and live reload the code.

To start the infrastructure components:

`./docker/bin/start-localDev.sh`

To stop infrastructure components:

`docker-compose -f docker/local/docker-compose-localDev.yml down --volumes`

#### CSV Format for Bulk Posting

`Time;Topic;OrderId;Payload
 2019-09-14 17:12:45.000;OrderPartsCancelled;9210084816;{"orderId":"9110460917","warehouse":"INGRAM_MICRO","isTest":false,"cancellationDate":"2019-10-12T13:26:03.000Z","orderLines":[{"ean":"5714492144498","lineNumber":1,"quantity":1,"cancelReason":"ITEM_NOT_AVAILABLE"}]}
 2019-09-14 17:15:46.000;OrderPartsCancelled;9410084734;{"orderId":"9210084816","warehouse":"INGRAM_MICRO","isTest":false,"cancellationDate":"2019-09-14T17:06:06.000Z","orderLines":[{"ean":"5713773659188","lineNumber":2,"quantity":1,"cancelReason":"ITEM_NOT_AVAILABLE"}]}
`

#### Local LDAP credentials

To test local LDAP credtentials, you can visit : https://github.com/rroemhild/docker-test-openldap to get a list of all
usernames provided by the test image. The following credentials will work:
`username: fry ; password: fry`

## Who do I talk to?
* SFS TEAM

