#!/bin/bash

BASEDIR=$(dirname "$0")

cd "$BASEDIR/../local/"

echo "Starting infrastructure components..."
docker-compose -f docker-compose-localDev.yml up
