package com.bestseller.kafka.tool.services.kafka;

import com.bestseller.kafka.tool.exceptions.KafkaConnectionException;
import com.bestseller.kafka.tool.models.common.RowData;
import com.bestseller.kafka.tool.models.enums.ConnectionStatus;
import com.bestseller.kafka.tool.models.enums.MessagePostStatus;
import com.bestseller.kafka.tool.models.request.MessagePostRequest;
import com.bestseller.kafka.tool.config.ConfigData;
import com.bestseller.kafka.tool.models.request.ConnectionCheckRequest;
import com.bestseller.kafka.tool.models.response.ConnectionCheckResponse;
import com.bestseller.kafka.tool.models.response.MessagePostResponse;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.KafkaAdminClient;
import org.apache.kafka.clients.admin.ListTopicsResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import static org.apache.kafka.clients.CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG;
import static org.apache.kafka.clients.CommonClientConfigs.CONNECTIONS_MAX_IDLE_MS_CONFIG;
import static org.apache.kafka.clients.CommonClientConfigs.REQUEST_TIMEOUT_MS_CONFIG;

@Service
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class KafkaService {

    @Autowired
    private ConfigData configData;

    @Value("#{'${spring.profiles.active}'.split(',')}")
    private List<String> envs;

    @Value("#{'${tool.env.local.topics}'.split(',')}")
    private List<String> topics;

    @Value("${kafka.broker.urls}")
    private String kafkaBrokerUrls;

    @Autowired
    private GenericJsonMessageProducer genericJsonMessageProducer;

    public ConnectionCheckResponse checkConnection() {
        ConnectionCheckResponse connectionCheckResponse = new ConnectionCheckResponse();
        connectionCheckResponse.setConnectionStatus(ConnectionStatus.CONNECTION_FAILED);
        try {
            List<String> topics = checkKafkaRunningStatus(kafkaBrokerUrls);
            if (!topics.isEmpty()) {
                connectionCheckResponse.setConnectionStatus(ConnectionStatus.CONNECTION_OK);
                connectionCheckResponse.setTopics(topics);
                configData.setBrokerUrls(kafkaBrokerUrls);
            }
        } catch (KafkaConnectionException e) {
            connectionCheckResponse.setConnectionStatus(ConnectionStatus.CONNECTION_FAILED);
        }
        return connectionCheckResponse;
    }

    public MessagePostResponse postMessage(MessagePostRequest messagePostRequest) {
        MessagePostResponse messagePostResponse = new MessagePostResponse();

        for (RowData rowData : messagePostRequest.getRowDataList()) {
            try {
                genericJsonMessageProducer.postMessage(rowData.getTopic(), rowData.getPayload());
                rowData.setMessagePostStatus(MessagePostStatus.POSTED);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Error : " + e.getMessage());
                rowData.setMessagePostStatus(MessagePostStatus.POST_FAILED);
            }
        }
        messagePostResponse.setRowDataList(messagePostRequest.getRowDataList());
        return messagePostResponse;
    }

    private List<String> checkKafkaRunningStatus(String brokerUrls) throws KafkaConnectionException {
        Properties properties = new Properties();
        properties.put(BOOTSTRAP_SERVERS_CONFIG, brokerUrls);
        properties.put(CONNECTIONS_MAX_IDLE_MS_CONFIG, 10000);
        properties.put(REQUEST_TIMEOUT_MS_CONFIG, 5000);
        try (AdminClient client = KafkaAdminClient.create(properties)) {
            ListTopicsResult topics = client.listTopics();
            Set<String> names = topics.names().get();
            if (names != null) {
                log.info("Connection is established");
            }
            return getTopicList(names);
        } catch (InterruptedException | ExecutionException e) {
            log.error("Error while making connection to kafka broker ",e);
            throw new KafkaConnectionException(e.getMessage());
        }
    }

    private List<String> getTopicList(Set<String> topicNames){
        if(!envs.contains("dev")){
            topics = new ArrayList<>();
            topics.addAll(topicNames);
        }
        Collections.sort(topics);
        topics.add(0, "History");
        return topics ;
    }
}
