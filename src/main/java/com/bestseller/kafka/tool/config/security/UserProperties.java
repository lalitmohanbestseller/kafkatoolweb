package com.bestseller.kafka.tool.config.security;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Property class to read user & pass config.
 */
@Configuration
@PropertySource("classpath:application.properties")
@ConfigurationProperties(prefix = "user")
@Getter
@Setter
public class UserProperties {

    private String username;
    private String password;

}
