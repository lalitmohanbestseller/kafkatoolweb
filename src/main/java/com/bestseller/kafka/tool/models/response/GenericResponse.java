package com.bestseller.kafka.tool.models.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class GenericResponse {
    private ConnectionCheckResponse connectionCheckResponse;
    private  MessageQueryResponse messageQueryResponse;
    private MessagePostResponse messagePostResponse;
}

