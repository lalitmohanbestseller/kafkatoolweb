module.config(function ($stateProvider) {
    $stateProvider
        .state('module', {
            abstract: true,
            // ...
            data: {
                requireLogin: true // this property will apply to all children of 'app'
            }
        })
});
